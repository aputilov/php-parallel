## Requirements
* [Composer](http://getcomposer.org)
* `php composer.phar install`

## Run Tests

`./vendor/bin/phpunit tests`

## Run Monte Carlo pi script
`php index.php {workersCount}`

## Комментарии
Написаны тесты только на WorkerDriver. Я не стал писать тесты на все классы, так как считаю, что для тестового задания будет излишне.

Существует `WorkerDriverInterface`, который описывает класс, который будет запускать код отдельно от текущего основного потока. Код, который исполняется в отдельном потоке описывается `WorkerInterface`.  
Общение между основным потоком и воркерами осуществляется посредством `MessagePipeInterface`, который реализует протокол общения. Каждое сообщение представляет собой `MessageInterface`.