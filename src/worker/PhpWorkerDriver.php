<?php

namespace m8rge\parallel\worker;


use m8rge\parallel\message\MessagePipe;
use m8rge\parallel\message\MessagePipeInterface;
use m8rge\parallel\Object;

class PhpWorkerDriver extends Object implements WorkerDriverInterface
{
    protected $phpBin = 'php';
    
    protected $autoloadFile;

    /**
     * Object initialize method
     */
    public function init()
    {
        parent::init();
        
        if (empty($this->autoloadFile)) {
            $this->autoloadFile = $this->getAutoloadFile();
        }
        if (!file_exists($this->autoloadFile)) {
            throw new WorkerDriverException('Autoload file doesn\'t exists');
        }
        exec($this->phpBin . ' --version', $output, $exitCode);
        if ($exitCode == 127) {
            throw new WorkerDriverException('Php binary file not found');
        }
    }

    /**
     * Start worker in parallel
     * @param WorkerInterface $worker
     * @return MessagePipeInterface
     */
    public function start(WorkerInterface $worker)
    {
        $descriptorSpec = [
            0 => ["pipe", "r"],
            1 => ["pipe", "w"], 
        ];
        
        $process = proc_open($this->phpBin .' ' . __DIR__.'/../entry.php', $descriptorSpec, $pipes, null, [
                'workerSerialized' => json_encode(serialize($worker), JSON_UNESCAPED_UNICODE), 
                'autoloadFile' => $this->getAutoloadFile()
            ]);
        
        return new MessagePipe([
            'readDescriptor' => $pipes[1],
            'writeDescriptor' => $pipes[0],
            'processDescriptor' => $process,
        ]);
    }

    /**
     * @return string
     */
    protected function getAutoloadFile()
    {
        $dir = realpath(__DIR__ . '/../..');
        
        if (file_exists($dir . '/autoload.php')) { // package installed with composer
            return $dir . '/autoload.php';
        } elseif (file_exists($dir . '/vendor/autoload.php')) { // package tests
            return $dir . '/vendor/autoload.php';
        }
        
        return '';
    }

    /**
     * Start several workers in parallel
     * @param WorkerInterface[] $workers
     * @return MessagePipeInterface[]
     * @throws WorkerDriverException
     */
    public function startInBatch($workers)
    {
        if (!is_array($workers)) {
            throw new WorkerDriverException('$workers must be array of WorkerInterface');
        }
        array_walk($workers, function($value) {
            if (!$value instanceof WorkerInterface) {
                throw new WorkerDriverException('$workers must be array of WorkerInterface');
            }
        });
        
        $pipes = [];
        foreach ($workers as $worker) {
            $pipes[] = $this->start($worker);
        }
        
        return $pipes;
    }
}