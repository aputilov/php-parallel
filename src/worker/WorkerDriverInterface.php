<?php

namespace m8rge\parallel\worker;


use m8rge\parallel\message\MessagePipeInterface;

interface WorkerDriverInterface
{
    /**
     * Start worker in parallel
     * @param WorkerInterface $worker
     * @return MessagePipeInterface
     */
    public function start(WorkerInterface $worker);

    /**
     * Start several workers in parallel
     * @param WorkerInterface[] $workers
     * @return MessagePipeInterface[]
     */
    public function startInBatch($workers);
}