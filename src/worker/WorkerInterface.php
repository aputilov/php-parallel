<?php

namespace m8rge\parallel\worker;


interface WorkerInterface
{
    /**
     * Worker main thread
     */
    public function start();
}