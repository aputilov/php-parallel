<?php

namespace m8rge\parallel\worker;


use m8rge\parallel\message\MessageInterface;
use m8rge\parallel\message\MessagePipeInterface;
use m8rge\parallel\Object;

class BaseWorker extends Object implements WorkerInterface
{
    /**
     * @var MessagePipeInterface
     */
    protected $messagePipe;

    /**
     * Object initialize method
     */
    public function init()
    {
        parent::init();
        
        if (!$this->messagePipe instanceof MessagePipeInterface) {
            throw new \RuntimeException('$messagePipe should be MessagePipeInterface instance');
        }
    }

    /**
     * Check for new messages and execute onMessage method
     */
    protected function checkForMessages()
    {
        $message = $this->messagePipe->read();
        if ($message) {
            $this->onMessage($message);
        }
    }

    /**
     * Worker main thread
     */
    public function start()
    {
        while ($this->step()) {
            $this->checkForMessages();
        }
    }

    /**
     * @return bool Whether next step exists. Return true to execute next step. Return false to stop worker.
     */
    protected function step()
    {
        return true;
    }

    /**
     * New message from master process event handler.
     * @param MessageInterface $message
     */
    protected function onMessage(MessageInterface $message)
    {
    }
}