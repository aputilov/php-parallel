<?php

require_once getenv('autoloadFile');

$workerSerialized = getenv('workerSerialized');
if (empty($workerSerialized)) {
    throw new Exception('Environment must contain non-empty workerSerialized variable');
}

/** @var \m8rge\parallel\worker\WorkerInterface $worker */
$worker = unserialize(json_decode($workerSerialized));
if ($worker instanceof \m8rge\parallel\worker\WorkerInterface) {
    $worker->start();
} else {
    throw new Exception('Worker must be instance of WorkerInterface');
}
