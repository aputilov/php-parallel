<?php

namespace m8rge\parallel;


/**
 * Object is the base class that implements setter feature and configure on construct.
 */
class Object
{
    /**
     * Object constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->configure($config);
        $this->init();
    }

    /**
     * Object initialize method
     */
    public function init()
    {
    }

    /**
     * @param array $config
     */
    protected function configure($config = [])
    {
        foreach ($config as $name => $value) {
            $this->__set($name, $value);
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $setter = 'set' . $name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } else {
            $this->$name = $value;
        }
    }
}