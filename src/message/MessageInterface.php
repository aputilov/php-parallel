<?php

namespace m8rge\parallel\message;


interface MessageInterface
{
    /**
     * @return mixed Payload data
     */
    public function getData();
}