<?php

namespace m8rge\parallel\message;


use m8rge\parallel\Object;

class Message extends Object implements MessageInterface
{
    /**
     * @var mixed Payload data
     */
    protected $data;
    
    /**
     * @return mixed Payload Data
     */
    public function getData()
    {
        return $this->data;
    }
}