<?php

namespace m8rge\parallel\message;


interface MessagePipeInterface
{
    /**
     * @param MessageInterface $message
     * @return bool
     */
    public function write(MessageInterface $message);

    /**
     * @return MessageInterface|null
     */
    public function read();

    /**
     * @return bool
     */
    public function isOpened();
}