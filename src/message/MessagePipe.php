<?php

namespace m8rge\parallel\message;


use m8rge\parallel\Object;

/**
 * Packet schema:
 * -------------------------------------------------------
 * | $header | payload data length | : | payload data |
 * -------------------------------------------------------
 */
class MessagePipe extends Object implements MessagePipeInterface
{
    /**
     * @var resource Default STDIN
     */
    public $readDescriptor;

    /**
     * @var resource Default STDOUT
     */
    public $writeDescriptor;

    /**
     * @var resource
     */
    public $processDescriptor;
    
    /**
     * @var string Message protocol header
     */
    private $header = "\036";

    /**
     * @var string
     */
    protected $buffer = '';

    /**
     * Object initialize method
     */
    public function init()
    {
        parent::init();

        if (empty($this->header)) {
            throw new MessagePipeException('$header shouldn\'t be empty');
        }
        if (is_resource($this->readDescriptor)) {
            stream_set_blocking($this->readDescriptor, 0);
        }
        if (is_resource($this->writeDescriptor)) {
            stream_set_blocking($this->writeDescriptor, 0);
        }
    }

    /**
     * @param MessageInterface $message
     * @return bool
     */
    public function write(MessageInterface $message)
    {
        if (!is_resource($this->writeDescriptor)) {
            $this->writeDescriptor = STDOUT;  // assigned here because of phpunit compatibility
            stream_set_blocking($this->writeDescriptor, 0);
        }
        $serialized = json_encode(serialize($message));

        $packet = $this->header . strlen($serialized) . ':' . $serialized;
        return fwrite($this->writeDescriptor, $packet) == strlen($packet);
    }

    /**
     * @return MessageInterface|null
     */
    public function read()
    {
        if (!is_resource($this->readDescriptor)) {
            $this->readDescriptor = STDIN; // assigned here because of phpunit compatibility
            stream_set_blocking($this->readDescriptor, 0);
        }
        $text = stream_get_contents($this->readDescriptor);
        $this->buffer .= $text;

        // check header start byte
        if (strlen($this->buffer) > 0 && strpos($this->buffer, $this->header) !== 0) {
            throw new MessagePipeException("Received wrong data: $this->buffer");
        }
        
        if (strlen($this->buffer) >= 5) { // minimum packet length 
            if (false !== $delimiterPos = strpos($this->buffer, ':')) {
                $packetLength = substr($this->buffer, strlen($this->header), $delimiterPos - 1);
                if (!is_numeric($packetLength)) {
                    throw new MessagePipeException("Received wrong data: $this->buffer");
                }
                $packet = substr($this->buffer, $delimiterPos + 1, $packetLength);
                $this->buffer = substr($this->buffer, $delimiterPos + 1 + $packetLength);
                if (false !== $message = unserialize(json_decode($packet))) {
                    return $message;
                } else {
                    throw new MessagePipeException("Received wrong packet: $packet");
                }
            }
        }

        return null;
    }

    function __destruct()
    {
        if (is_resource($this->readDescriptor)) {
            fclose($this->readDescriptor);
        }
        if (is_resource($this->writeDescriptor)) {
            fclose($this->writeDescriptor);
        }
        if (is_resource($this->processDescriptor)) {
            proc_close($this->processDescriptor);
        }
    }

    /**
     * @return bool
     */
    public function isOpened()
    {
        if (is_resource($this->processDescriptor)) {
            $processInfo = proc_get_status($this->processDescriptor);
            return $processInfo['running'];
        }
        
        return true;
    }
}