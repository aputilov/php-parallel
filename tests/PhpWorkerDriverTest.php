<?php

namespace m8rge\tests;


use m8rge\parallel\message\Message;
use m8rge\parallel\message\MessageInterface;
use m8rge\parallel\message\MessagePipeInterface;
use m8rge\parallel\worker\PhpWorkerDriver;
use m8rge\tests\worker\EchoWorker;
use m8rge\tests\worker\PrintWorker;
use m8rge\tests\worker\SleepWorker;

class PhpWorkerDriverTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param MessagePipeInterface $pipe
     * @return MessageInterface|null
     */
    protected function readSync($pipe)
    {
        $i = 0;
        while(!$message = $pipe->read()) {
            if ($i++ == 10) {
                break;
            }
            usleep($i*100000); //0.1 sec
        }
        
        return $message;
    }

    /**
     * @param MessagePipeInterface $pipe
     */
    protected function waitForClose($pipe)
    {
        $i = 0;
        while($opened = $pipe->isOpened()) {
            if ($i++ == 10) {
                break;
            }
            usleep($i*100000);
        }
        
        $this->assertFalse($opened, "Child process must be closed");
    }
    
    public function testSeparateProcess()
    {
        $workerDriver = new PhpWorkerDriver();
        $pipe = $workerDriver->start(new SleepWorker());
        usleep(500000); // 0.5 sec
        $this->assertTrue($pipe->isOpened(), "Child process must exists");
        
        $this->waitForClose($pipe);
    }
    
    public function testEcho()
    {
        $workerDriver = new PhpWorkerDriver();
        $pipe = $workerDriver->start(new EchoWorker());
        $this->assertTrue($pipe->isOpened(), "Child process must exists");
        
        $written = $pipe->write(new Message(['data' => '123']));
        $this->assertTrue($written, 'Message was not written to pipe');
        
        $message = $this->readSync($pipe);
        $this->assertInstanceOf(MessageInterface::class, $message);
        $this->assertEquals('123', $message->getData());

        $written = $pipe->write(new Message(['data' => '0']));
        $this->assertTrue($written, 'Message was not written to pipe');
        
        $this->waitForClose($pipe);
    }

    public function testStartParameters()
    {
        $workerDriver = new PhpWorkerDriver();
        $pipe = $workerDriver->start(new PrintWorker(['string' => 'qwe']));
        $this->waitForClose($pipe);
        
        $message = $this->readSync($pipe);
        $this->assertInstanceOf(MessageInterface::class, $message);
        $this->assertEquals('qwe', $message->getData());
    }

    public function testSeveralThreads()
    {
        $workerDriver = new PhpWorkerDriver();
        $pipes = $workerDriver->startInBatch([
            new PrintWorker(['string' => '123']),
            new PrintWorker(['string' => '456']),
            new PrintWorker(['string' => '789']),
        ]);

        $message = $this->readSync($pipes[0]);
        $this->assertInstanceOf(MessageInterface::class, $message);
        $this->assertEquals('123', $message->getData());

        $message = $this->readSync($pipes[1]);
        $this->assertInstanceOf(MessageInterface::class, $message);
        $this->assertEquals('456', $message->getData());

        $message = $this->readSync($pipes[2]);
        $this->assertInstanceOf(MessageInterface::class, $message);
        $this->assertEquals('789', $message->getData());
    }
}
