<?php

namespace m8rge\tests\worker;


use m8rge\parallel\message\MessagePipe;
use m8rge\parallel\worker\BaseWorker;

class SleepWorker extends BaseWorker
{
    /**
     * Object initialize method
     */
    public function init()
    {
        $this->messagePipe = new MessagePipe();

        parent::init();
    }

    /**
     * Worker main thread
     */
    public function start()
    {
        sleep(1);
    }
}