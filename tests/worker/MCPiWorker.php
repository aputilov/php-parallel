<?php

namespace m8rge\tests\worker;

use m8rge\parallel\message\Message;
use m8rge\parallel\message\MessageInterface;
use m8rge\parallel\message\MessagePipe;
use m8rge\parallel\worker\BaseWorker;

class MCPiWorker extends BaseWorker
{
    /**
     * @var int
     */
    protected $step = 1;

    /**
     * @var int 
     */
    protected $steps;

    /**
     * @var int
     */
    protected $hits = 0;

    /**
     * Object initialize method
     */
    public function init()
    {
        $this->messagePipe = new MessagePipe();

        parent::init();
    }

    /**
     * @return bool Whether next step exists. Return true to execute next step. Return false to stop worker.
     */
    protected function step()
    {
        $x = mt_rand() / mt_getrandmax();
        $y = mt_rand() / mt_getrandmax();
        if ($x * $x + $y * $y < 1) {
            $this->hits++;
        }

        $stop = $this->step++ == $this->steps;
        if ($stop) {
            $this->messagePipe->write(new Message(['data' => $this->getCalculatedPi()]));
        }
        return !$stop;
    }

    /**
     * New message from master process event handler.
     * @param MessageInterface $message
     */
    protected function onMessage(MessageInterface $message)
    {
        if ($message->getData() == 'getResult') {
            $this->messagePipe->write(new Message(['data' => $this->getCalculatedPi()]));
        }
    }

    /**
     * @return float
     */
    public function getCalculatedPi()
    {
        return $this->hits / $this->step * 4;
    }
}