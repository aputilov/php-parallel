<?php

namespace m8rge\tests\worker;


use m8rge\parallel\message\Message;
use m8rge\parallel\message\MessagePipe;
use m8rge\parallel\worker\BaseWorker;

class PrintWorker extends BaseWorker
{
    public $string;

    /**
     * Object initialize method
     */
    public function init()
    {
        $this->messagePipe = new MessagePipe();

        parent::init();
    }

    /**
     * @return bool Whether next step exists. Return true to execute next step. Return false to stop worker.
     */
    protected function step()
    {
        $this->messagePipe->write(new Message(['data' => $this->string]));
        
        return false;
    }
}