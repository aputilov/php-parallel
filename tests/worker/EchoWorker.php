<?php

namespace m8rge\tests\worker;

use m8rge\parallel\message\MessageInterface;
use m8rge\parallel\message\MessagePipe;
use m8rge\parallel\worker\BaseWorker;

class EchoWorker extends BaseWorker
{
    public $run = true;

    /**
     * Object initialize method
     */
    public function init()
    {
        $this->messagePipe = new MessagePipe();
        
        parent::init();
    }

    /**
     * @return bool Whether next step exists. Return true to execute next step. Return false to stop worker.
     */
    protected function step()
    {
        return $this->run;
    }

    /**
     * New message from master process event handler.
     * @param MessageInterface $message
     */
    protected function onMessage(MessageInterface $message)
    {
        if ($message->getData() == '0') {
            $this->run = false;
            return;
        }
        $this->messagePipe->write($message);
    }
}