<?php

include_once 'vendor/autoload.php';

use m8rge\parallel\message\Message;
use m8rge\parallel\message\MessageInterface;
use m8rge\parallel\worker\PhpWorkerDriver;
use m8rge\tests\worker\MCPiWorker;

if (empty($argv[1]) || !is_numeric($argv[1])) {
    die("usage: index.php {processCount}\n");
}

$workerDriver = new PhpWorkerDriver();
$workers = [];
$workersCount = $argv[1];
$askTimeout = [];
for ($i = 0; $i < $workersCount; $i++) {
    $steps = mt_rand(1000, 100000);
    $askTimeout[] = mt_rand(1000000, 1000000 * $workersCount) / 1000000;
    $workers[] = new MCPiWorker(['steps' => $steps]);
}
$startTime = microtime(true);
$pipes = $workerDriver->startInBatch($workers);

echo "Started $workersCount workers\n";
$results = [];
do {
    $haveAnyWorkerActive = false;
    foreach ($pipes as $i => $pipe) {
        $haveAnyWorkerActive |= $pipe->isOpened();

        if (is_numeric($askTimeout[$i]) && microtime(true) > $startTime + $askTimeout[$i]) {
            if ($pipe->write(new Message(['data' => 'getResult']))) {
                echo "Worker $i asked after $askTimeout[$i] sec\n";
            }
            $askTimeout[$i] = null;
        }
        $message = $pipe->read();
        if ($message instanceof MessageInterface) {
            $results[$i] = $message->getData();
            echo "Worker $i returned pi value: {$results[$i]}\n";
        }
        usleep(10);
    }
} while ($haveAnyWorkerActive);

$result = array_sum($results) / count($results);
$error = abs(pi() / $result * 100 - 100);
$error = sprintf("%.2f", $error);
$elapsed = microtime(true) - $startTime;
$elapsed = sprintf("%.1f", $elapsed);
echo "----------------\nResult calculated pi value in $elapsed sec: $result Error: $error%\n";
